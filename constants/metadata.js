export const METADATA = {
  INDEX: {
    title: 'Autoguru',
    description: 'Autoguru website',
    keywords: 'Autoguru, Car, Automotive',
  },
  ABOUT: {
    title: 'About Us | Autoguru',
    description: 'About us',
    keywords: 'Autoguru, Car, Automotive',
  }
}