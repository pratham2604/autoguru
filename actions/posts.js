import GHOST_API from '../lib/ghostServer';

export async function getPosts() {
  return await GHOST_API.posts.browse({
    include: 'tags, author',
    limit: 'all',
  }).catch(err => {
    console.error(err);
  });
}