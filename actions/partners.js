import axios from "axios";

export default async () => {
  const response = await axios.get("https://autoguru-admin.firebaseapp.com/api/autoguru-partners");
  const { data = [] } = response;
  console.log(data);
  return data;
}