# Autoguru

## Installing Dependencies

```bash
npm install
# or
yarn install
```

## Getting Started
Running development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Production deployment

```bash
1. Add your IP in security group for autoguru
2. ssh into server using following commant
   ssh -i autoguru-deploy.pem ubuntu@autoguru.in
3. cd autoguru/
4. git pull origin master
5. yarn build
6. pm2 restart frontend
```
