import Layout from '../components/layout/layout';
import Home from '../modules/home/UI/index';
import { METADATA } from '../constants/metadata';
import { getPosts } from '../actions/posts';

const IndexPage = (props) => {
  const { description, keywords, title } = METADATA.INDEX;
  const { posts = [] } = props;

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <Home posts={posts} />
    </Layout>
  );
}

IndexPage.getInitialProps = async () => {
  const posts = await getPosts();
  return { posts: posts }
}

export default IndexPage;