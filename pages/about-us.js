import Layout from '../components/layout/layout';
import About from '../modules/about/UI/index';
import { METADATA } from '../constants/metadata';

const IndexPage = (props) => {
  const { description, keywords, title } = METADATA.ABOUT;

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <About />
    </Layout>
  );
}

export default IndexPage;