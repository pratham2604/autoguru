import Layout from '../components/layout/layout';
import UnderstandingModule from '../modules/understanding/UI/index';
import { METADATA } from '../constants/metadata';
import { getPosts } from '../actions/posts';

const IndexPage = (props) => {
  const { description, keywords, title } = METADATA.INDEX;
  const { posts = [] } = props;

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <UnderstandingModule posts={posts} />
    </Layout>
  );
}

IndexPage.getInitialProps = async () => {
  const posts = await getPosts();
  return { posts: posts }
}

export default IndexPage;