import Layout from '../components/layout/layout';
import CarReviews from '../modules/carReviews/UI/index';
import { METADATA } from '../constants/metadata';
import { getPosts } from '../actions/posts';

const CarReviewsPage = (props) => {
  const { description, keywords, title } = METADATA.INDEX;
  const { posts = [] } = props;

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <CarReviews posts={posts} />
    </Layout>
  );
}

CarReviewsPage.getInitialProps = async () => {
  const posts = await getPosts();
  return { posts: posts }
}

export default CarReviewsPage;