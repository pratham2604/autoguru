import Layout from '../components/layout/layout';
import MotorSportsNewsPage from '../modules/motorSportsBlogs/UI/index';
import { METADATA } from '../constants/metadata';
import { getPosts } from '../actions/posts';

const MotorSportsNewsBlog = (props) => {
  const { description, keywords, title } = METADATA.INDEX;
  const { posts = [] } = props;

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <MotorSportsNewsPage posts={posts} />
    </Layout>
  );
}

MotorSportsNewsBlog.getInitialProps = async () => {
  const posts = await getPosts();
  return { posts: posts }
}

export default MotorSportsNewsBlog;