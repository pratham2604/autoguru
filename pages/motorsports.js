import Layout from '../components/layout/layout';
import MotorSports from '../modules/motorSports/UI/index';
import { METADATA } from '../constants/metadata';
import { getPosts } from '../actions/posts';
import getPartners from '../actions/partners';

const MotorSportsPage = (props) => {
  const { description, keywords, title } = METADATA.INDEX;
  const { posts = [], profiles } = props;

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <MotorSports posts={posts} profiles={profiles} />
    </Layout>
  );
}

MotorSportsPage.getInitialProps = async () => {
  const posts = await getPosts();
  const partners = await getPartners();
  return {
    posts: posts,
    profiles: partners.data || []
  }
}

export default MotorSportsPage;