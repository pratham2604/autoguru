import Layout from '../components/layout/layout';
import Blogs from '../modules/blogs/UI/index';
import { METADATA } from '../constants/metadata';
import { getPosts } from '../actions/posts';

const BlogsPage = (props) => {
  const { description, keywords, title } = METADATA.INDEX;
  const { posts = [] } = props;

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <Blogs posts={posts} />
    </Layout>
  );
}

BlogsPage.getInitialProps = async () => {
  const posts = await getPosts();
  return { posts: posts }
}

export default BlogsPage;