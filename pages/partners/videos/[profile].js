import { useRouter } from 'next/router';
import Error from 'next/error'
import Layout from '../../../components/layout/layout';
import VideoPage from '../../../modules/profile/UI/videoPage';
import { METADATA } from '../../../constants/metadata';
import getPartners from '../../../actions/partners';

const Profile = (props) => {
  const { description, keywords, title } = METADATA.INDEX;
  const { profiles = [] } = props;
  const router = useRouter();
  const { profile } = router.query;
  const profilePresent = profiles.find(profileData => profileData.url === profile);
  if (!profilePresent) {
    return <Error statusCode="404" />
  }

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <VideoPage data={profilePresent} />
    </Layout>
  );
}

Profile.getInitialProps = async () => {
  const partners = await getPartners();
  return {
    profiles: partners.data || []
  }
}

export default Profile;