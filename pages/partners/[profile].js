import { useRouter } from 'next/router';
import Error from 'next/error'
import Layout from '../../components/layout/layout';
import ProfilePage from '../../modules/profile/UI/index';
import { METADATA } from '../../constants/metadata';
import { getPosts } from '../../actions/posts';
import getPartners from '../../actions/partners';

const Profile = (props) => {
  const { description, keywords, title } = METADATA.INDEX;
  const { profiles = [], posts = [] } = props;
  const router = useRouter();
  const { profile } = router.query;
  const profilePresent = profiles.find(profileData => profileData.url === profile);
  if (!profilePresent) {
    return <Error statusCode="404" />
  }

  return (
    <Layout pageTitle={title} description={description} keywords={keywords}>
      <ProfilePage data={profilePresent} posts={posts} />
    </Layout>
  );
}

Profile.getInitialProps = async () => {
  const posts = await getPosts();
  const partners = await getPartners();
  return {
    posts: posts,
    profiles: partners.data || []
  }
}

export default Profile;