import { Component } from "react";
import { Segment, Container, Grid, Header, List, Image, Input, Button, Select, Icon } from 'semantic-ui-react';
import Logo from '../../public/assets/logo.svg';
import Link from 'next/link'

export default class extends Component {
  state = {
    type: '',
    email: '',
    message: '',
  }

  onChange = (event, target) => {
    const { name, value } = target;
    this.setState({
      [name]: value
    })
  }

  render() {
    const options = [{
      text: 'Car Buying Advice',
      value: 'Car Buying Advice',
    }, {
      text: 'Content Creation',
      value: 'Content Creation',
    }, {
      text: 'Partnerships',
      value: 'Partnerships',
    }, {
      text: 'Marketing & Brand Solutions',
      value: 'Marketing & Brand Solutions',
    }];
    const { type, email, message } = this.state;
    const submitHref = `mailto:info@autoguru.in?&subject=${type}&body=${message}`

    return (
      <Segment inverted vertical className="footer-container">
        <Container>
          <Grid inverted stackable>
            <Grid.Row>
              <Grid.Column width={4} className="info-container">
                <div className="logo-container">
                  <Image src={Logo} size="tiny" className="logo"/>
                  <Header as="h2" inverted>AutoGuru</Header>
                </div>
                <Header as="h5" inverted as="a" href="mailto:info@autoguru.in" className="mail-to">
                  info@autoguru.in
                </Header>
                <div className="copyright">© 2020 AutoGuru.</div>
                <div className="social-media-icons">
                  <a href="https://www.facebook.com/autoguruindia/" target="_blank">
                    <Icon className="menu-right" name="facebook"/>
                  </a>
                  <a href="https://www.instagram.com/autoguruindia/" target="_blank">
                    <Icon className="menu-right" name="instagram" />
                  </a>
                  <a href="https://www.linkedin.com/company/autoguru-india/" target="_blank">
                    <Icon className="menu-right" name="linkedin" />
                  </a>
                  <a href="https://twitter.com/AutoGuru_India" target="_blank">
                    <Icon className="menu-right" name="twitter"/>
                  </a>
                </div>
              </Grid.Column>
              <Grid.Column width={3}>
                <Header inverted as='h3' content='What we do' />
                <List link inverted>
                  <List.Item as={Link} href="/our-blogs">
                    <a className="footer-menu-link">Blogs</a>
                  </List.Item>
                  <List.Item as={Link} href="/motorsports">
                    <a className="footer-menu-link">Motorsport</a>
                  </List.Item>
                </List>
              </Grid.Column>
              <Grid.Column width={3}>
                <Header inverted as='h3' content='Who we are' />
                <List link inverted>
                  <List.Item as={Link} href="/about-us">
                    <a className="footer-menu-link">About Us</a>
                  </List.Item>
                </List>
              </Grid.Column>
              <Grid.Column width={4}>
                <Header as='h4' inverted>
                  Want to get in touch ?
                </Header>
                <Grid className="subscribe-container">
                  <Grid.Row>
                    <Grid.Column computer={16} tablet={16} mobile={16}>
                      <Select fluid options={options} className="email" inverted onChange={this.onChange} name="type"/>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column computer={16} tablet={16} mobile={16}>
                      <Input fluid placeholder='Email Address' className="email" onChange={this.onChange} name="email"/>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column computer={16} tablet={16} mobile={16}>
                      <Input fluid placeholder='Message' className="email" onChange={this.onChange} name="message"/>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column computer={16} tablet={16} mobile={16}>
                      <Button fluid className="submit" primary as="a" href={submitHref}>Submit</Button>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </Segment>
    )
  }
}