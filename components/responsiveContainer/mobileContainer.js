import { Responsive } from 'semantic-ui-react';

const getWidth = () => {
  const isSSR = typeof window === 'undefined'
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}

export default (props) => (
  <Responsive getWidth={getWidth} maxWidth={Responsive.onlyMobile.maxWidth}>
    {props.children}
  </Responsive>
);