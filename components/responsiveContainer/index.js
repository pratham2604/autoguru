import Desktop from './desktopContainer';
import Mobile from './mobileContainer';

export default {
  Desktop,
  Mobile,
};