import React, { Component } from 'react';
import { Segment, Menu, Container, Image } from 'semantic-ui-react';
import Logo from '../../public/assets/logo.svg';
import Link from 'next/link';

export default class extends Component {
  render() {
    return (
      <Segment inverted textAlign='center' vertical className="mobile-header">
        <Menu fixed="top" inverted={true} pointing={true} secondary={true} size='large'>
          <Container>
            <Menu.Item as='a' className="logo-container">
              <Link href="/">
                <Image src={Logo} size="tiny" className="logo"/>
              </Link>
            </Menu.Item>
            <RIGHT_MENU />
          </Container>
        </Menu>
        <Menu inverted={true} pointing={true} secondary={true} size='large' className="buffer-menu">
          <Container>
            <Menu.Item as='a' className="logo-container">
              <Image src={Logo} size="tiny" className="logo"/>
            </Menu.Item>
            <RIGHT_MENU />
          </Container>
        </Menu>
      </Segment>
    )
  }
}

const RIGHT_MENU = () => (
  <Menu.Item position='right' className="menu-right-container">
    <div>
      <a className="menu-right" href="/about-us">About</a>
      <a className="menu-right" href="/our-blogs">Blogs</a>
      <a className="menu-right" href="/motorsports">Motorsports</a>
    </div>
  </Menu.Item>
)