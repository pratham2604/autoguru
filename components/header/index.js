import { Fragment } from 'react';
import DesktopHeader from './desktopHeader';
import MobileHeader from './mobileHeader';

export default () => (
  <div className="header-container">
    <div className="desktop-header-container">
      <DesktopHeader />
    </div>
    <div className="mobile-header-container">
      <MobileHeader />
    </div>
  </div>
)