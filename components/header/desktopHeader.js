import React, { Component } from 'react';
import Link from 'next/link';
import { Segment, Menu, Container, Image, Icon, Header } from 'semantic-ui-react';
import Logo from '../../public/assets/logo.svg';

export default class extends Component {
  render() {
    return (
      <Segment inverted textAlign='center' className="desktop-header" vertical>
        <Menu fixed="top" inverted={true} pointing={true} secondary={true} size='large'>
          <Container>
            <Menu.Item className="logo-container">
              <Link href="/">
                <Image src={Logo} size="tiny" className="logo"/>
              </Link>
              <Link href="/">
                <Header as="h1" className="logo-title" inverted>AutoGuru</Header>
              </Link>
            </Menu.Item>
            <RIGHT_MENU />
          </Container>
        </Menu>
        <Menu inverted={true} pointing={true} secondary={true} size='large' className="buffer-menu">
          <Container>
            <Menu.Item as='a' className="logo-container">
              <Image src={Logo} size="tiny" className="logo"/>
            </Menu.Item>
            <RIGHT_MENU />
          </Container>
        </Menu>
      </Segment>
    )
  }
}

const RIGHT_MENU = () => (
  <Menu.Item position='right' className="menu-right-container">
    <div>
      <Link href="/about-us">
        <a className="menu-right">About Us</a>
      </Link>
      <Link href="/our-blogs">
        <a className="menu-right">Blogs</a>
      </Link>
      <Link href="/motorsports">
        <a className="menu-right">Motorsports</a>
      </Link>
      <a href="https://www.facebook.com/autoguruindia/" target="_blank">
        <Icon className="menu-right" name="facebook"/>
      </a>
      <a href="https://www.instagram.com/autoguruindia/" target="_blank">
        <Icon className="menu-right" name="instagram" />
      </a>
      <a href="https://www.linkedin.com/company/autoguru-india/" target="_blank">
        <Icon className="menu-right" name="linkedin" />
      </a>
      <a href="https://twitter.com/AutoGuru_India" target="_blank">
        <Icon className="menu-right" name="twitter"/>
      </a>
    </div>
  </Menu.Item>
)