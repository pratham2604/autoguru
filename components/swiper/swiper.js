import { Fragment } from 'react';
import { Responsive } from 'semantic-ui-react';
import Swiper from 'react-id-swiper';

export default (props) => {
  const { children } = props;
  return (
    <Fragment>
      <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.maxWidth}>
        <Swiper {...desktopParams}>
          {children.map((node, index) => (
            <div key={index}>
              {node}
            </div>
          ))}
        </Swiper>
      </Responsive>
      <Responsive getWidth={getWidth} maxWidth={Responsive.onlyTablet.maxWidth} minWidth={Responsive.onlyMobile.maxWidth}>
        <Swiper {...tabletParams}>
          {children.map((node, index) => (
            <div key={index}>
              {node}
            </div>
          ))}
        </Swiper>
      </Responsive>
      <Responsive getWidth={getWidth} maxWidth={Responsive.onlyMobile.maxWidth}>
        <Swiper {...mobileParams}>
          {children.map((node, index) => (
            <div key={index}>
              {node}
            </div>
          ))}
        </Swiper>
      </Responsive>
    </Fragment>
  )
}

const getWidth = () => {
  const isSSR = typeof window === 'undefined';
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
}

const desktopParams = {
  slidesPerView: 4,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  }
};

const tabletParams = {
  slidesPerView: 2,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  }
}

const mobileParams = {
  slidesPerView: 1,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  }
}