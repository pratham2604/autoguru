import { Component } from 'react';
import { Image, Icon, Grid, Rating } from 'semantic-ui-react';
import Link from 'next/link';
import DEFAULT_IMAGE from '../../public/assets/article/default.png';
import moment from 'moment';

export default class Article extends Component {
  render() {
    const { image, title, description, link, published_at, reading_time, layout, article = {} } = this.props;
    const { featured = false } = article;
    if (layout === 'horizontal') {
      return <HorizontalArticle {...this.props} />
    }

    return (
      <div>
        <div className="article-container">
          <div className="image-container">
            <Image src={image || DEFAULT_IMAGE} size="large"/>
          </div>
          <div className="info-container">
            <div className="title">
              <div style={{flex: 1}}>{title}</div>
              <div>
                {featured && <Icon name="star" color="yellow"></Icon>}
              </div>
            </div>
            {(published_at || reading_time) &&
              <div className="post-info">
                <div className="publish-date">
                  <Icon name="calendar"></Icon>
                  <span>{moment(published_at).format('DD-MM-YYYY')}</span>
                </div>
                <div className="reading-time">
                  <Icon name="clock"></Icon>
                  <span>{reading_time} mins</span>
                </div>
              </div>
            }
            <div className="description">
              {description}
            </div>
          </div>
          {link &&
            <div className="view-more">
              <a href={link || '/'} className="link">More</a>
            </div>
          }
        </div>
      </div>
    )
  }
}

const HorizontalArticle = (props) => {
  const { image, title, description, link, published_at, reading_time, layout, linkedIn } = props;
  return (
    <div>
        <div className="article-container horizontal-article">
          <Image src={image || DEFAULT_IMAGE} size="medium" floated='left'/>
          <div className="info-container">
            <div className="title">
              {title}
              {linkedIn &&
                <span style={{float: 'right'}}>
                  <a href={linkedIn} target="_blank">
                    <Icon className="menu-right" name="linkedin" />
                  </a>
                </span>
              }
            </div>
            {(published_at || reading_time) &&
              <div className="post-info">
                <div className="publish-date">
                  <Icon name="calendar"></Icon>
                  <span>{moment(published_at).format('DD-MM-YYYY')}</span>
                </div>
                <div className="reading-time">
                  <Icon name="clock"></Icon>
                  <span>{reading_time} mins</span>
                </div>
              </div>
            }
            <div className="description">
              {description}
            </div>
          </div>
          {link &&
            <div className="view-more">
              <a href={link || '/'} className="link">More</a>
            </div>
          }
        </div>
      </div>
  )
}