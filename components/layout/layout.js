import React, { Component } from 'react';
import Head from 'next/head';
import { Responsive } from 'semantic-ui-react';
import favicon from '../../public/favicon.ico';
import Footer from '../footer/index';
import Header from '../header/index';
import '../../styles/index.scss';
import 'semantic-ui-css/semantic.min.css';
import 'swiper/css/swiper.css';

const getWidth = () => {
  const isSSR = typeof window === 'undefined'
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}

export default class Layout extends Component {
  render() {
    const {
      pageTitle = 'Autoguru',
      description = '',
      keywords = '',
      children
    } = this.props;

    return (
      <div className="layout">
        <Head>
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165570526-1"></script>
          <script
            dangerouslySetInnerHTML={{__html: `window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-165570526-1');`}}
            rel="preconnect"
            defer
            async
          />
          <title>{pageTitle}</title>
          <meta name="description" content={description}></meta>
          <meta name="keywords" content={keywords}></meta>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <link rel="icon" href={favicon} sizes="16x16 32x32" type="image/png"></link>
          <link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&display=swap" rel="stylesheet"></link>
          <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/icon.min.css" rel="stylesheet"></link>
        </Head>
        <Header/>
          {children}
        <Footer/>
      </div>
    );
  }
}