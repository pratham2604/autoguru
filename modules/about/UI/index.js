import { Header, Container, Grid, List, Icon, Image } from 'semantic-ui-react';
import Logo from '../../../public/assets/logo.svg';

export default () => {
  return (
    <div className="component-container chakras-container">
      <div className="about-us-container"></div>
      <Container>
        <Grid stackable centered className="title-container">
          <Grid.Row>
            <Grid.Column width={16} textAlign="center">
              <Header as="h1" className="component-title">About Us</Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={10}>
              <br/>
              <br/>
              <Header as="h1" className="component-title">
                What We Do:
              </Header>
              <Header as="h3" inverted className="description">
                AutoGuru is a content driven organisation with the aim of ‘Making Cars Simple’ for all the
                stakeholders involved. We create written content catering to the automotive and
                motorsport industry. We are also actively working with car dealerships, service providers,
                OES, race teams, race drivers and motorsport IPs towards helping them share their stories
                and meet brand objectives.
              </Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={10}>
              <br/>
              <Header as="h1" className="component-title">
                Vision:
              </Header>
              <Header as="h3" inverted className="description">
                Making cars simple for all stakeholders of the car and motorsport industry.
              </Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={10}>
              <br/>
              <Header as="h1" className="component-title">
                Mission:
              </Header>
              <Header as="h3" inverted className="description">
                To make cars simple by creating an informative and unbiased content platform for all
                stakeholders in the car and motorsport industry.
              </Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={10}>
              <Header as="h1" className="component-title">
                Services:
              </Header>
              <Header as="h3" inverted className="description">
                <List inverted>
                  <List.Item>
                    <Icon name='triangle right' />
                    <List.Content>
                      <List.Header as="h3">
                        Content Creation & Strategy
                      </List.Header>
                      <List.Description>
                        Creating content for our clients to meet their brand objectives by way of leveraging
                        the content we create for them.
                      </List.Description>
                      <br/>
                    </List.Content>
                  </List.Item>
                  <List.Item>
                    <Icon name='triangle right' />
                    <List.Content>
                      <List.Header as="h3">Brand Outreach Programs</List.Header>
                      <List.Description>
                        Ideation, planning and execution of brand outreach program for schools and colleges.
                      </List.Description>
                      <br/>
                    </List.Content>
                  </List.Item>
                  <List.Item>
                    <Icon name='triangle right' />
                    <List.Content>
                      <List.Header as="h3">Events & Workshops.</List.Header>
                    </List.Content>
                  </List.Item>
                  <List.Item>
                    <Icon name='triangle right' />
                    <List.Content>
                      <List.Header as="h3">Talent Management and Client Servicing</List.Header>
                    </List.Content>
                  </List.Item>
                </List>
              </Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={10}>
              <Header as="h1" className="component-title">
                Partners:
              </Header>
              <Header as="h3" inverted className="description">
                <List inverted>
                  <List.Item>
                    <Icon name='triangle right' />
                    <List.Content>
                      <List.Header as="h3">SCU Motorsports</List.Header>
                    </List.Content>
                  </List.Item>
                  <List.Item>
                    <Icon name='triangle right' />
                    <List.Content>
                      <List.Header as="h3">My Super Car</List.Header>
                    </List.Content>
                  </List.Item>
                </List>
              </Header>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid stackable centered>
          
        </Grid>
      </Container>
    </div>
  )
}