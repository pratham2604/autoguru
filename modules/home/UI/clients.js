import { Header, Container, Grid, Image } from 'semantic-ui-react';
import Swiper from '../../../components/swiper/swiper';

const Client1 = 'static/images/iitbombay/logo.png';
const Client2 = 'static/images/pravega/logo.png';
const Client3 = 'static/images/kshatriya/logo.png';
const Client4 = 'static/images/mysupercar/logo.png';
const Client5 = 'static/images/scu/logo.png';

const clients = [Client1, Client2, Client3, Client4, Client5];

export default () => (
  <div className="component-container client-container">
    <Container>
      <Grid stackable className="title-container">
        <Grid.Row>
          <Grid.Column>
            <Header as="h1" className="component-title">
              Partners and Clients
            </Header>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      {/* <Grid stackable centered>
        <Grid.Row>
          {clients.map((client, index) => 
            <Grid.Column key={index} width={4}>
              <Image src={client} size="small" className="client-logo"/>
            </Grid.Column>
          )}
        </Grid.Row>
      </Grid> */}
      <Swiper>
        {clients.map((client, index) => <div><Image key={index} src={client} size="small" className="client-logo"/></div>)}
      </Swiper>
    </Container>
  </div>
);
