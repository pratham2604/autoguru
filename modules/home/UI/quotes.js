import { Header, Container, Grid, Image } from 'semantic-ui-react';
import { Component } from 'react';
import quotes from '../data/quotes';

export default class extends Component {
  state = {
    index: 0,
  }

  generateRandom = () => {
    return Math.floor((Math.random() * quotes.length));
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      let nextIndex = this.generateRandom();
      while(nextIndex === this.state.index) {
        nextIndex = this.generateRandom();
      }
      this.setState({
        index: nextIndex,
      });
    }, 15000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { index } = this.state;
    const randomQuote = quotes[index];
    return (
      <div className="quotes-container">
        <Container>
          <Header as="h1" className="component-title title" textAlign="center" style={{textDecoration: 'underline'}}>
            Did you know ?
          </Header>
          <Header as="h1" className="component-title" inverted>
            {randomQuote}
          </Header>
        </Container>
      </div>
    )
  }
}
