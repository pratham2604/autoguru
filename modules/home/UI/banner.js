import { Component } from 'react';
import { Grid, Header, Input, Button, Image, Container } from 'semantic-ui-react';
const IllustrationSrc = 'static/images/home/Illustration.png'

export default class extends Component {
  render() {
    const bodyMessage = `I would like to subscribe newsletter`;
    const subject = `Subscribe NewsLetter`;
    const submitHref = `mailto:info@autoguru.in?&subject=${subject}&body=${bodyMessage}`;
    return (
      <div className="component-container banner-container">
        <Container>
          <Grid stackable reversed="computer">
            <Grid.Row>
              <Grid.Column width="8">
                <Image src={IllustrationSrc} size="big" />
              </Grid.Column>
              <Grid.Column width="8" className="title-container">
                <Header as="h1" className="component-title">
                  Making Cars Simple
                </Header>
                <div className="description">
                  If you like our content, Sign up for our all exclusive newsletter for exciting news around cars, 
                  motorsport, racers and everything that has an engine
                </div>
                <Grid className="subscribe-container">
                  <Grid.Row>
                    <Grid.Column computer={12} tablet={16} mobile={16}>
                      <Input fluid placeholder='Email Address' className="email"/>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row>
                    <Grid.Column computer={12} tablet={16} mobile={16}>
                      <Button fluid className="submit" primary as="a" href={submitHref}>Submit</Button>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      </div>
    )
  }
}