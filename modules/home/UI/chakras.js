import { Header, Container, Grid, Image, Icon, Responsive } from 'semantic-ui-react';
import Chakra1 from '../../../public/assets/chakras/1.svg';
import Chakra2 from '../../../public/assets/chakras/2.svg';
import Chakra3 from '../../../public/assets/chakras/3.svg';
import Chakra4 from '../../../public/assets/chakras/4.svg';
import Chakra5 from '../../../public/assets/chakras/5.svg';
import Chakra6 from '../../../public/assets/chakras/6.svg';
import Chakra7 from '../../../public/assets/chakras/7.svg';

const CHAKRAS = [{
  title: 'Root Chakra - Simplicity',
  description: `Simplicity is at the core of what AutoGuru stands for. AutoGuru wants to make
  cars simple and easy to understand for all stakeholders of the car and motorsport
  industry.`,
  image: Chakra1,
}, {
  title: 'Sacral Chakra - Direct Open Communication',
  description: `AutoGuru keeps and maintains direct open communication with all its customers,
  stakeholders and readers.`,
  image: Chakra2,
}, {
  title: 'Solar Plexus - Transparency',
  description: `AutoGuru believes in transparency with regard to the communication delivered to
  its customers and readers. The content created, brand communication and
  message delivered by AutoGuru are transparent and crystal clear.`,
  image: Chakra3,
}, {
  title: 'Heart Chakra - Neutrality',
  description: `The content created and communicated delivered by AutoGuru is neutral and
  without any bias to any OEM, OES, service provider, motorsport team, race driver,
  IP etc.`,
  image: Chakra4,
}, {
  title: 'Throat Chakra - Honesty',
  description: `AutoGuru is true and honest about all opinions, suggestions and thoughts
  provided via its content or brand communication.`,
  image: Chakra5,
}, {
  title: 'Third eye chakra - In Depth Knowledge and Research',
  description: `AutoGuru conducts in depth research based on extensive knowledge about the
  content created for its customers and readers alike.`,
  image: Chakra6,
}, {
  title: 'Crown Chakra - Guidance and Expert Advice for Customers',
  description: `AutoGuru believes in being a part of the end to end journey of its customers by
  directing them through the process of buying a car, keeping their requirement as
  top priority. AutoGuru being an expert in the automotive domain is always
  available to resolve all car related queries.`,
  image: Chakra7,
}]

export default () => (
  <div className="component-container chakras-container">
    <Container>
      <Grid stackable className="title-container">
        <Grid.Row>
          <Grid.Column>
            <Header as="h1" className="component-title" textAlign="center">
              7 Chakras of AutoGuru
            </Header>
            <Header as="h1" className="component-title" textAlign="center" style={{textDecoration: 'underline'}}>
              What we stand for
            </Header>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
        <Grid stackable centered>
          {CHAKRAS.map((chaktra, index) => {
            const { image, title, description } = chaktra;
            return (
              <Grid.Row className="chakra-item" key={index}>
                <Grid.Column width="7">
                  {index % 2 !== 0 &&
                    <div className="info-container">
                      <div className="info">
                        <div className="info-tile">{title}</div>
                        <div className="info-description">{description}</div>
                      </div>
                    </div>
                  }
                </Grid.Column>
                <Grid.Column width="2">
                  <Image src={image} className="chakra-image" size="tiny"/>
                </Grid.Column>
                <Grid.Column width="7">
                  {index % 2 === 0 &&
                    <div className="info-container">
                      <div className="info">
                        <div className="info-tile">{title}</div>
                        <div className="info-description">{description}</div>
                      </div>
                    </div>
                  }
                </Grid.Column>
              </Grid.Row>
            )
          })}
        </Grid>
      </Responsive>
      <Responsive getWidth={getWidth} maxWidth={Responsive.onlyMobile.maxWidth}>
        <Grid stackable centered>
          {CHAKRAS.map((chaktra, index) => {
            const { image, title, description } = chaktra;
            return (
              <Grid.Row className="chakra-item" key={index}>
                <Grid.Column width="2">
                  <Image src={image} className="chakra-image" size="tiny"/>
                </Grid.Column>
                <Grid.Column width="14">
                  <div className="info-container">
                    <div className="info">
                      <div className="info-tile">{title}</div>
                      <div className="info-description">{description}</div>
                    </div>
                  </div>
                </Grid.Column>
              </Grid.Row>
            )
          })}
        </Grid>
      </Responsive>
    </Container>
  </div>
);

const getWidth = () => {
  const isSSR = typeof window === 'undefined';
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}
