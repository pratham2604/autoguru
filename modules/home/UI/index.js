import Banner from './banner';
import Blogs from './blogs';
import Chakras from './chakras';
import News from './news';
import Team from './team';
import Clients from './clients';
import Quotes from './quotes';

export default (props) => (
  <div>
    <Banner />
    <News {...props}/>
    <Chakras />
    <Blogs {...props}/>
    <Team />
    <Quotes />
    <Clients />
  </div>
)