import { Header, Container, Grid } from 'semantic-ui-react';
import Article from '../../../components/article/article';
const ATIV_IMG = 'static/images/team/ativ.jpg'
const JASH_IMG = 'static/images/team/jash.jpg';

export default () => (
  <div className="component-container team-container">
    <Container>
      <Grid stackable className="title-container">
        <Grid.Row>
          <Grid.Column computer={5} tablet={9} mobile={16}>
            <Header as="h1" className="component-title">
              Founders
            </Header>
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Grid stackable centered>
        <Grid.Row>
          <Grid.Column width="8">
            <Article layout="horizontal" image={GURUS[0].image} title={GURUS[0].title} description={GURUS[0].description} linkedIn={GURUS[0].linkedIn} />
          </Grid.Column>
          <Grid.Column width="8">
            <Article layout="horizontal" image={GURUS[1].image} title={GURUS[1].title} description={GURUS[1].description} linkedIn={GURUS[1].linkedIn} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  </div>
);

const GURUS = [{
  title: 'Ativ Shah',
  image: ATIV_IMG,
  linkedIn: 'https://www.linkedin.com/in/ativ-shah-768197101/',
  description: (
    <div>
      <div className="content-para">
        Ativ found his love for cars at a very young age. What started off as a hobby of playing with dinky
        cars soon transpired to collecting model cars. Those who know him well can agree if there is
        anything he loves apart from family and friends it is cars!
      </div>
      <div className="content-para">
        Ativ has a marketing and business development background having worked in the IT, sport and
        education industries. Starting his career with IT firm Sundaram Technologies serving the
        pharmaceutical industry, he then went on to work for Sports For All (SFA). At SFA he worked very
        closely with schools and educational institutes across the country and also got a chance to work in
        one of India’s largest sport events. Though he is not an engineer by education, Ativ is technically
        sound when it comes to everything related to cars! At AutoGuru, Ativ is responsible for content and
        marketing. An avid learner and a keen reader he has a vision for making the car, motorsport
        industry simple and transparent for all stakeholders.
      </div>
    </div>
  )
}, {
  title: 'Jash Shah',
  image: JASH_IMG,
  linkedIn: 'https://www.linkedin.com/in/jash-shah-4169746b/',
  description: (
    <div>
      <div className="content-para">
        As a young boy fuelled with immense passion for cars, Jash always found the sound of engine to be
        more soothing than any other music to his ears.
      </div>
      <div className="content-para">
        His journey revolves around the auto industry, engulfing knowledge about a vast array of
        touchpoints to garner in-depth insights. With Automobile Research being his core strength, he is
        currently working as an Investment Analyst at Val-Q. He is a former employee of J.M. Financials
        where he was working as an Equity Research Analyst for automobile and banking sector. He started
        his career at Deloitte, closely understanding Risk Assurance. He is an MBA from a B-School, but
        always with A-grades!
      </div>
      <div className="content-para">
        Passion and perseverance are his strongest virtues. He’s the one who promises order in a world of
        chaos. A go-getter by nature, his role at AutoGuru involves administration and execution.
      </div>
    </div>
  )
}]
