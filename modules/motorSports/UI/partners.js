import { Header, Container, Grid } from 'semantic-ui-react';
import Article from '../../../components/article/article';

export default (props) => {
  const { profiles = [], type, title } = props;
  const partnerData = profiles.filter(profile => profile.type === type).map(partner => Object.assign({}, {
    link: `/partners/${partner.url}`,
    image: partner.banner,
    title: partner.title,
    description: partner.shortDescription,
  }))

  return (
    <div className="component-container alternate-component-container">
      <Container>
        <Grid stackable columns='equal' centered className="title-container">
          <Grid.Row>
            <Grid.Column>
              <Header as="h1" inverted className="component-title" textAlign="left">
                {title}
              </Header>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row className="car-reviews-container">
            {partnerData.map((partner, index) => {
              return (
                <Grid.Column computer={5} tablet={9} mobile={16}>
                  <Article
                    key={index}
                    link={partner.link}
                    image={partner.image}
                    title={partner.title}
                    description={partner.description}
                  />
                </Grid.Column>
              )
            })}
          </Grid.Row>
        </Grid>
      </Container>
    </div>
  )
};
