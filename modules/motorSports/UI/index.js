import Blogs from './blogs';
import Partners from './partners';

export default (props) => (
  <div>
    <Partners {...props} type="partner" title="Partners"/>
    <Partners {...props} type="client" title="Clients"/>
    <Blogs {...props}/>
  </div>
)