import { Header, Container, Grid } from 'semantic-ui-react';
import Link from 'next/link';
import Swiper from '../../../components/swiper/swiper';
import Article from '../../../components/article/article';

export default (props) => {
  const { posts = [] } = props;
  const news = posts.filter(post => {
    const { tags } = post;
    return tags.some(tag => tag.slug === 'latest-news');
  }).sort((a, b) => {
    const aDate = new Date(a.published_at).valueOf();
    const bDate = new Date(b.published_at).valueOf();
    return bDate - aDate;
  }).slice(0, 5);

  return (
    <div className="component-container">
      <Container>
        <Grid stackable className="title-container">
          <Grid.Row>
            <Grid.Column computer={5} tablet={9} mobile={16}>
              <Header as="h1" className="component-title">
                Latest Updates
              </Header>
            </Grid.Column>
            <Grid.Column computer={3} tablet={3} mobile={16}>
              <div className="view-more-link">
                <Link href="/latest-updates">
                  <a className="link">See all</a>
                </Link>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <div className="news-swiper-container">
          <Swiper>
            {news.map((point, index) => (
              <Article
                key={index}
                link={point.url}
                image={point.feature_image}
                title={point.title}
                description={point.excerpt}
                published_at={point.published_at}
                reading_time={point.reading_time}
                article={point}
              />
            ))}
          </Swiper>
        </div>
      </Container>
    </div>
  );
}
