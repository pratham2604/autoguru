import Updates from './updates';
import Understanding from './understanding';
import Reviews from './carReviews';
import MotorsportNews from '../../motorSports/UI/blogs';

export default (props) => (
  <div>
    <Updates {...props}/>
    <Understanding {...props}/>
    <Reviews {...props}/>
    <MotorsportNews {...props} />
  </div>
)