const imagePath = '../static/images/kshatriya/';

const data = {
  url: 'kshatriya',
  images: [
    `${imagePath}1.jpg`,
    `${imagePath}2.png`,
    `${imagePath}3.jpg`,
    `${imagePath}4.jpg`,
  ],
  banner: `${imagePath}4.jpg`,
  logo: `${imagePath}logo.png`,
  title: 'Team Kshatriya',
  slug: 'team-kshatriya',
  shortDescription: 'The official BAJA SAE India Team of Vellore Institute of Technology, Vellore.',
  description: `The aim of Team Kshatriya is to be the best BAJA SAE team in India. Our objective is to
  design, build and test an All-Terrain Vehicle which will be fast, manoeuvrable, and durable
  to run on any terrain and to ultimately win the coveted BAJA SAE INDIA.`,
  links: {
    facebook: 'https://www.facebook.com/Team.Kshatriya/',
    twitter: 'https://twitter.com/TeamKshatriya',
    linkedIn: 'https://www.linkedin.com/company/team-kshatriya/',
    instagram: 'https://www.instagram.com/team.kshatriya/?hl=en',
    other: '#'
  },
  achievements: [
  ],
  faqs: [{
    question: 'What led to the formation of Team Kshatriya?',
    answer: `Team Kshatriya was founded in 2006 by a group of Mechanical Engineering students
    of Vellore Institute of Technology, Vellore. It was one of the first all student
    motorsport team in India and was the first Indian team to participate in BAJA SAE
    South Africa. We participated in BAJA USA in the subsequent years, but after the
    inception of BAJA SAE India, we have been primarily focusing on that event.`,
  }, {
    question: 'What Team Kshatriya do?',
    answer: `Team Kshatriya works towards the design, manufacture, test and validation All-
    Terrain vehicle and participates in Baja SAE India and Enduro Student India which
    are intercollegiate engineering design competition for undergraduate and graduate
    engineering students.`,
  }, {
    question: 'What are Aims and Objectives of Team Kshatriya?',
    answer: `The aim of Team Kshatriya is to be the best BAJA SAE team in India. Team
    Kshatriya’s objective is to design, build and test an All-Terrain Vehicle which will be
    fast, manoeuvrable, and durable to run on any terrain and to ultimately win the
    coveted BAJA SAE INDIA.`,
  }, {
    question: 'Has Team Kshatriya participated in an International competition?',
    answer: `Team Kshatriya is one of the oldest BAJA team of India. Team Kshatriya was the first
    Indian BAJA team to participate in BAJA SAE South Africa in 2008 and BAJA SAE
    USA in 2009.`,
  }, {
    question: 'What are the features of Team Kshatriya’s current car – Vikrant?',
    answer: `
    <div class="ui bulleted list">
      <div class="item">
        It is a combustion vehicle powered by a single-cylinder 305cc 10HP air cooled, Briggs &amp; Stratton OHV Intake Model (engine is event specified, all teams have to use the same engine with no other modifications).
      </div>
      <div class="item">
        The car has a custom designed Continuous Variable Transmission system and a max speed of 60+ kmph.
      </div>
      <div class="item">
        Has a turning radius of 1.8 meters.
      </div>
      <div class="item">
        Custom Master Cylinder, Brake Callipers and Rotors.
      </div>
      <div class="item">
        Afco 63 Series Dual Set-up Suspension System with a ground clearanceof 15 inches.
      </div>
    </div>
    `,
  }, {
    question: 'How can one become a member of Team Kshatriya?',
    answer: `Only bona fide students of VIT, Vellore can become members of Team Kshatriya. A
    student of VIT, Vellore who is in 1 st year of his/her B.Tech can be member of Team. A
    student needs to clear Requirement Paper, Personal Interview and Technical task to
    be member of Team Kshatriya.`
  }],
  timeline: [{
    year: 2020,
    title: 'BAJA SAE India',
    events: [
      '51 st out of 429 participating teams.',
      '30 th in Design Presentation.',
      '29 th  in Acceleration.',
      '11 th  in Business Presentation.',
      '13 th in Rock Crawl.',
    ]
  }, {
    year: 2019,
    title: 'BAJA SAE India',
    events: [
      '27 th out of 451 participating teams.',
      '13 th in Maneuverability.',
      '39 th in Design Presentation.',
      '26 th  in Acceleration.',
      '26 th in Suspension.',
      '38 th in Endurance.',
      '29 th in Acceleration',
      '11 th  in Business Presentation.'
    ]
  }, {
    year: 2018,
    title: 'Enduro Student India',
    events: [
      '1 st in Lightest car of event.',
      '3 rd in Sprint.',
      '21 st in Design.',
      '15 th in Dirtx.',
      '9 th in Endurance.',
      '17 th in Maneuverability.',
      '6 th in Sales.',
      '8 th in Cost.'
    ]
  }, {
    year: 2017,
    title: 'BAJA SAE India',
    events: [
      '6 th of the 350 participating teams.',
      '3 rd in Maneuverability.',
      '3 rd in Design Presentation.',
      '5 th in Sled Pull.',
      '26 th in Suspension.',
      '38 th in Endurance.',
      '29 th in Acceleration',
      '11 th  in Business Presentation.'
    ]
  }, {
    year: 2016,
    title: 'BAJA SAE India',
    events: [
      '7 th out of 379 participating teams.',
      '5 th in Design presentation.',
      '6 th in Static Evaluation.',
      '7 th in Hill Climbing.',
    ]
  }, {
    year: 2015,
    title: 'BAJA SAE India',
    events: [
      '3 rd of the 451 participating teams.',
      '3 rd in Sales Presentation.',
      '3 rd in Hill Climb.',
      '5 th in Endurance Race.',
    ]
  }]
}

export default data;