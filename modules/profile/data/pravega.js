const imagePath = '../static/images/pravega/';

const data = {
  url: 'pravega',
  images: [
    `${imagePath}1.jpg`,
    `${imagePath}2.jpg`,
    `${imagePath}3.JPG`,
    `${imagePath}4.JPG`,
  ],
  banner: `${imagePath}1.jpg`,
  logo: `${imagePath}logo.png`,
  title: 'Pravega Racing',
  slug: 'pravega-racing',
  shortDescription: 'The official combustion Formula Student team of Vellore Institute of Technology, Vellore.',
  description: `Pravega Racing is the official combustion Formula Student team for Vellore Institute of
    Technology, Vellore. We are a group of budding engineers who design, manufacture and
    race a single seater racecar in various International and National Formula Student events.
    We are constantly innovating and moving forward to reach towards our goal of being the
    best Formula Student team in the world.`,
  links: {
    facebook: 'https://www.facebook.com/pravegaracing/',
    twitter: 'https://twitter.com/pravegaracing?lang=en',
    linkedIn: 'https://www.linkedin.com/company/pravega-racing./',
    instagram: 'https://www.instagram.com/pravega_racing/?hl=en',
    other: '#'
  },
  achievements: [
    'Winners of Formula Bharat 2017.',
    'Winners - BASF Best Use Of 3D Printing award at Formula Student Germany 2019.',
    'Highest ever points scored by an Indian team in the Engineering Design event at Formula Student Australasia 2018.',
    'Best Indian team at Formula Student Germany 2016.',
  ],
  faqs: [{
    question: 'Which events does Pravega Racing visit and participate in all around the world?',
    answer: `Pravega Racing participates in various National and International events around the
      world. We recently went to the Hockenheim Ring in Germany for the biggest event
      of the season Formula Student Germany. This year we had planned to go to 2 events,
      first time in Pravega history, to Formula Student Austria and Formula Student East.`,
  }, {
    question: 'How did Pravega Racing Start?',
    answer: `Pravega Racing was founded in 2009 by our first captain Mr. Vikramjit Singh, who
    had the simple idea of putting his theoretical knowledge and foundation from his
    college and applying it in the practical real world. He formed Pravega Racing and it
    has been growing ever since.`,
  }, {
    question: 'Where does Pravega Racing get funding from?',
    answer: `We are proud representatives of our sponsors, partners and believers. Our
    management team works hard to keep the team afloat and support the technical
    engineers to give them the best the team can get their hands on. Furthermore, we
    host a Fund-raising campaign during which we aim to reach a goal for our budget for
    the next season.`,
  }, {
    question: 'What do you need to become a Pravega Engineer?',
    answer: `A Pravega team member is not just an engineer, but should be capable to be a
    hardworking, dedicated and intelligent team member. Efficiency and on the spot
    thinking are key to being a valuable member of Pravega Racing hence that is what
    we look for the most. The ability to learn and perfect as quickly as possible is what’s
    most important.`,
  }, {
    question: 'What are some of the technical features of the race car?',
    answer: `For the past few seasons, we have been using the engine from a Honda CBR 600 RR,
    but now as times are changing and engineering is pacing forward, we have decided
    to use a KTM390 as the future of the team for making our race car even lighter. The
    aim of the team is to build a race car with these goals in mind- Lightness, reliable and
    well-integrated. Hence, we design our own suspension geometry, chassis and
    ergonomics for the driver from scratch.`,
  }],
  timeline: [{
    year: 2019,
    title: 'Formula Student Germany',
    events: [
      'Won the BASF - Best use of 3D printing award.',
      '15 th place in Business Presentation.',
      '15 th place in Engineering Design.',
      'Ranked overall 42 nd in the world.',
    ]
  }, {
    year: 2018,
    title: 'Formula Student Australasia',
    events: [
      'Highest ever points scored by an Indian team at the Engineering Design event.',
      'Won the FSAE-A encouragement award.',
      'Ranked 4 th  in Engineering design.',
      'Ranked 9 th  in acceleration.',
      'Ranked 14 th  overall.',
    ]
  }, {
    year: 2017,
    title: 'Formula Bharat',
    events: [
      'Winners of Formula Bharat 2017.',
      '1 st place in Autocross.',
      '1 st place in Business Presentation',
      '1 st place in Cost Event.',
      '2 nd place in Skid Pad.',
      '2 nd place in Endurance.',
      'Winners of the Special Award for Spirit of FSI.',
      'Winners of the Best Driver Award.',
    ]
  }, {
    year: 2016,
    title: 'Formula Student Germany',
    events: [
      'Best Indian team at Formula Student Germany 2016.',
      '13 th place in Business Presentation.',
      '39 th place in Design Event.',
      '40 th place in Cost Report.',
    ]
  }]
}

export default data;