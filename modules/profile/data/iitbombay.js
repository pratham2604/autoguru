const imagePath = '../static/images/iitbombay/';

const data = {
  url: 'iit-bombay',
  images: [
    `${imagePath}0.jpeg`,
    `${imagePath}1.jpeg`,
    `${imagePath}2.jpeg`,
    `${imagePath}3.jpg`,
    `${imagePath}4.jpg`,
  ],
  banner: `${imagePath}1.jpeg`,
  logo: `${imagePath}logo.png`,
  title: 'IIT Bombay Racing',
  slug: 'iit-bombay-racing',
  shortDescription: 'Revolutionize electric mobility in India focusing on sustainable technologies and innovations',
  description: `IIT Bombay Racing is the official Formula Student (electric) team of the Indian Institute of
    Technology, Mumbai. Team IIT Bombay Racing are the first college racing team from India to
    go electric and are pioneers in this field. IIT Bombay Racing are India’s best electric college
    racing team and regularly participate at Formula Student UK. Their aim is to revolutionize
    electric mobility in India focusing on sustainable technologies and innovations.`,
  links: {
    facebook: 'https://www.facebook.com/iitbracing/',
    twitter: 'https://twitter.com/iitb_racing?lang=en',
    linkedIn: 'https://www.linkedin.com/company/iit-bombay-racing/',
    instagram: 'https://www.instagram.com/iitbombayracing/',
    other: '#'
  },
  achievements: [
    'India’s first 3D printed titanium uprights (in collaboration with Wipro).',
    '1 st college racing team to make an electric car in India (2012).',
    'First Indian electric racing car with monocoque.',
    'First Indian team to use an electronic differential.',
    'Highest rank achieved by an Indian electric racing team at FSUK.',
    'First Indian team to complete the endurance event at FSUK.',
    '3 rd place among electric cars at the FSUK Autox event.',
    '2 nd place in Cost Report at FSUK 2009.'
  ],
  faqs: [],
  timeline: [{
    title: '2019',
    events: [
      'EVoK - Carbon fibre battery box.',
      '4th Overall rank among electric cars In FSUK 2019.',
    ]
  }, {
    title: '2018',
    events: [
      'EVoX – World’s first 3D Printed Titanium Uprights, Aerodynamics Package.',
      '1st position in Business event at Formula Bharat 2018.',
    ]
  }, {
    title: '2016',
    events: [
      'Orca - Implemented carbon fibre Bodyworks and Carbon fibre A-Arms',
    ]
  }, {
    title: '2015',
    events: [
      'Evo4 - 1st Indian car to complete the endurance event at FSUK.',
    ]
  }]
}

export default data;