import IITBombay from './iitbombay';
import Pravega from './pravega';
import Kshatriya from './kshatriya';
import MySuperCar from './mysupercar';
import SCU from './scu';

export default [
  IITBombay,
  Pravega,
  Kshatriya,
  MySuperCar,
  SCU,
];