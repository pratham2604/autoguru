const imagePath = '../static/images/scu/';

const data = {
  url: 'scu-motorsport',
  images: [
    `${imagePath}1.png`,
    `${imagePath}2.png`,
    `${imagePath}3.png`
  ],
  banner: `${imagePath}1.png`,
  logo: `${imagePath}logo.png`,
  title: 'SCU Motorsport',
  slug: 'scu-motorsport',
  shortDescription: 'The driving force behind motorsport education.',
  description: `SCU Motorsport are developing a race series specifically for colleges and universities. At college
  level; which typically teaches the practical/’hands on’ skills that mechanics and practical engineers
  need, the race series will be linked to a formal qualification. For Universities the race series has
  classes that will allow modification of the car to support design and research that can be linked to
  Degree Level learning.
  SCU Motorsport are designing a single seater car specifically for the series, with a focus on learning,
  as none of the currently available single seater race cars meet the needs to support the programme.
  This will be the first race series in the world to focus on training mechanics and engineers, rather
  than driver development.`,
  links: {
    facebook: 'https://www.facebook.com/SCUMotorsport/',
    twitter: '#',
    linkedIn: 'https://www.linkedin.com/company/scu-motorsport-ltd/',
    instagram: '#',
    other: '#'
  },
  faqs: [{
    question: 'Do you need a degree to get a job in motorsport?',
    answer: `No, you don’t need a degree to get a job in motorsport or to perform tasks like suspension
    set up, check brakes, refuel the car, change wheels and tyres, spanner check the car or
    analyse data. There are two areas within motorsport engineering; ‘hands on’, i.e., doing
    practical tasks – mechanics and engineers, and technical roles such as design and
    development where a degree is useful, but again not necessary. We would go as far as to say
    that even if you want to take a degree, practical experience will make you a better engineer.`,
  }, {
    question: 'What is it that SCU Motorsport are developing?',
    answer: `The key thing that SCU Motorsport are developing is a race series specifically for colleges
    and universities which will allow them to compete against each other. For college level the
    race series will be linked to formal qualification, which will be developed, written and
    produced by SCU Motorsport, in conjunction with the motorsport sector. SCU Motorsport
    are also going to develop a Virtual Reality workshop that will allow individuals to log in and
    perform various activities such as ‘race car set up’ on a virtual race car.`,
  }, {
    question: 'How do you know that you are taking the correct approach?',
    answer: `In 2019, SCU Motorsport received funding from the Education Charity UFI to undertake
    research to explore the extent of the skills gap, and validate the approach SCU Motorsport
    are taking. SCU Motorsport has spoken to over 50 race teams at the National and
    International level, manufacturers and the supply chain and the message was clear, the
    current approach is not working and the programme SCU Motorsport are developing has the
    potential to deliver the correct skills and also provide invaluable and relevant experience.`
  }, {
    question: 'Why have you not made the race series just for Electric Cars?',
    answer: `SCU Motorsport is focusing on training motorsport mechanics and engineers that will be
    entering the sport in the short, medium, and long term. Electric Vehicle technology will form
    part of the training programme, as will emerging technology. Petrol Engines will be fitted to
    new vehicles until 2035, and will continue to be used long after this, under historic and
    classic racing. Even now there are major shortages for Classic and Historic mechanics that
    needs to be addressed; and this is focussed on technology from 100 years ago. We need to
    address the past, present and future.`
  }, {
    question: 'What is more important – practical experience or theoretical understanding?',
    answer: `Without a doubt ‘Practical Experience’, and the reason we say this is that we have had a
    number of universities say to us that they have had foundation level students turn up to
    study, and they are involved in design work for Formula Student. These students have never
    picked up a spanner – so they do not understand the needs of a mechanic, and therefore do
    not understand the design envelope. CAD will allow the individual to design virtually
    anything, but if the person who has to fit it to the car can’t get his spanner on to adjust it, or
    fit it then it is a poorly designed component. For this reason, the universities asked SCU
    Motorsport if they could compete in the race series.
    Many Universities are starting to realise this, and on Sunday 4th August 2019, we were
    watching the British Touring Car Championship (BTCC) on the television and heard the
    presenters talking about future mechanics and engineers in motorsport – it was a great
    feature, and the key message “practical experience that can only be achieved at the circuit in
    a race environment”.<a href="https://www.youtube.com/watch?time_continue=3&v=RWvyjz_a7M0&feature=emb_title" target="_blank">
    https://www.youtube.com/watch?time_continue=3&v=RWvyjz_a7M0&feature=emb_title</a> (Video used with permission from ITV4 to SCU
     Motorsport Ltd for Education Purposes)`,
    setHTML: true,
  }],
  videos: [
    'EITZm4NybW4',
    '6zJQKn6ojl8',
  ]
}

export default data;