const imagePath = '../static/images/mysupercar/';

const data = {
  url: 'my-super-car',
  images: [
    `${imagePath}1.jpg`,
    `${imagePath}2.jpg`,
    `${imagePath}3.jpeg`
  ],
  banner: `${imagePath}1.jpg`,
  logo: `${imagePath}logo.png`,
  title: 'MySuperCar',
  slug: 'mysupercar',
  shortDescription: `MySuperCar are UK based Motorsport Live Stream broadcasters working with multiple race series such as the Classic Touring Cars, F1000, and the Caterham Graduates.`,
  description: `MySuperCar are UK based Motorsport Live Stream broadcasters working with multiple race
  series such as the Classic Touring Cars, F1000, and the Caterham Graduates. Formed in 2018
  by sports journalist Ian Waterhouse, film editor Alain van Zweel, and entrepreneur Leon van
  Zweel, MySuperCar quickly established themselves at the forefront of live Motorsport
  broadcasting with a behind the scenes approach billed as ‘F1 Style Media to the Club
  Circuit’. Find out more about MySuperCar and the main company ContentLIVE by
  visiting www.contentlive.net.`,
  links: {
    facebook: 'https://www.facebook.com/HelloMySuperCar/',
    twitter: 'https://twitter.com/MySuperCar1',
    linkedIn: 'https://www.linkedin.com/in/ian-waterhouse-1ba1896b/',
    instagram: 'https://www.instagram.com/mysupercar.co.uk/?hl=en',
    other: '#'
  },
  achievementsTitle: 'Three notable interviews we have carried out:',
  achievements: [
    'Eric Boullier - Former Renault, Lotus and McLaren Formula One team boss',
    'Nick Tandy – 2015 Le Mans winner',
    'Rob Gravett – 1990 British Touring Car Champion/ 1997 Independent British Touring Car Champion',
  ],
  faqs: [{
    question: 'Where can we watch LIVE motorsport?',
    answer: `We broadcast via each series social media channels but all Live Motorsport content can be watched on MySuperCar’s Facebook page and YouTube channel.`,
  }, {
    question: 'Who are the team',
    answer: `Ian Waterhouse is the presenter and co-founder of MySuperCar. Leon Van Zweel is the technician and co-founder and Alain van Zweel is co-founder and editor.`,
  }, {
    question: 'How can we find out more?',
    answer: `You can visit www.mysupercar.co.uk or www.contentlive.net – ContentLIVE is the operating name of the business with MySuperCar the Motorsport arm of the business.`
  }],
  services: [{
    title: 'Live Stream Broadcasting',
    description: `At MySuperCar we have developed our ‘F1 Style Media to the Club Circuit’ to give fresh
    behind the scenes look at Motorsport without missing the track action. See the things you
    don’t normally get to see with MySuperCar LIVE broadcasts.`,
  }, {
    title: 'Motorsport Marketing Program',
    description: `We work with a number of race drivers, teams, and series to promote and raise their profile.
    Through additional video content, engaging race reports and PR, we provide an all-in-one
    marketing solution to Motorsport.`,
  }, {
    title: 'RAMP – Responsive Automotive Program',
    description: `Our Responsive Automotive Marketing Program is designed to generate leads in the
    automotive sector using our video content and expert lead generation skills with a
    designated and proven lead generation campaign combining Email Marketing, Video, Blogs,
    and Paid Advertising.`
  }, {
    title: 'Car stories with a twist',
    description: `With love a story with a difference and some of our car stories involve a 1936 Chevrolet and
    a murdered American Gangster, the man keeping the Lancia Stratos alive in Europe, and
    true story of the Dodge Viper.`
  }],
  videos: [
    'dHa3CYZ5_OM',
    '-SWcJ88_k9E',
    '6TAoS8d4ZN0',
    'eRdaf065dPc',
    'Ysba1Dcor2g',
    'mGV-hP3jWpY',
    '0VE3d6-SU10',
    'lO5m4e8Y1tI',
    '7seaKmEDm3o',
    'PURY8KeKGIQ',
    '84hoRwetGSQ',
    'IVRRzzMYIe0',
    'jhaYED5jpdY',
    'xiKUhAS1lgY',
    'pYpJG6doLOE',
    'ogs-HkbH6RY',
    'UR27cGCINx8',
    '-8b0EVgm0ek',
    'eDXqqG4iZXw',
    'Co63FyZtsb4',
    'mdx9x4QkSw8',
    'CoXr0jcC8r4',
    '14e0iibB6zY',
    'Y7teuTyU7v8',
    'yRwTGYZTN3M',
    'uDZdps-Q2C8',
    'JEIEVfrpwbQ',
    'nTotN3_NJa4',
    '8y281h3yQoA',
    'WLLDJTFauf0',
    'V_VjQSFZ8YE',
    'pR2vH-PhuBo',
    '7hjDQ3VUL-4',
    'cbQwvDwJ-Bw',
    'aMjNkXFtNvo',
    'TFkB3sbXp-8',
    'it09tK6Kpng',
    'eb_dYHl6QzI',
    'yx8BCLycaew',
    'Ee5mmrmx9b0',
    '_HbLSq9TaVs',
    '-XIoB727eGE',
    'qUl1QBmJnOM',
    '57wYib39r08',
    'RQS19DRCf_c',
    '0_ajbun9t0Q',
    'mNvDHZjFn84',
    'yOS5C31fVjM',
    'LpXqudHrvpE',
    'AZ7-TO_eHgs',
    '2VqGgjbqNJs',
    'K2ComLYHBuw',
    'URRS1qZ-zaM',
    '_baPscmwam8',
    'i9rhukmYym0',
    'ogH0vdM9Vc0',
    'gL4NDF16hRw',
    'uXqzQF0C85A',
    'GdwAeaK6zCs',
    'qUvWjTVWYc0',
    '0VTlFoZivwE',
    'DXFzlCcMqNM',
    'c7W5WU6fahk',
    '2CgQxhpgTM4',
    '81yw3at6ZHc',
    'SkUyFVlcng8',
  ]
}

export default data;