import { Header, Container, Grid } from 'semantic-ui-react';
import Slider from './slider';
import Details from './details';
import Achievements from './achievements';
import TimeLine from './timeline';
import FAQ from './faqs';
import Blogs from './blogs';
import Videos from './videos';
import Services from './services';

export default (props) => {
  const { data = {}, posts } = props;
  const { images = [], achievements = [], faqs = [], timeline = [], achievementsTitle, url, slug, videos, services = [] } = data;
  return (
    <div>
      <div className="component-container profile-component-container">
        <Container>
          <Slider images={images} />
          <Details data={data} />
        </Container>
      </div>
      {(timeline.length > 0 || services.length > 0 || achievements.length > 0) &&
        <div className="profile-component-container">
          <Container>
            <Grid stackable>
              <Grid.Row>
                <Grid.Column width={7}>
                  <TimeLine timeline={timeline}/>
                  <Services services={services} />
                </Grid.Column>
                {achievements.length > 0 && 
                  <Grid.Column width={9}>
                    <Achievements achievements={achievements} achievementsTitle={achievementsTitle} />
                  </Grid.Column>
                }
              </Grid.Row>
            </Grid>
          </Container>
        </div>
      }
      <Blogs posts={posts} slug={slug} />
      <Videos videos={videos} url={url} />
      <FAQ faqs={faqs} />
    </div>
  )
};
