import { Header, List } from 'semantic-ui-react';

export default (props) => {
  const { services = [] } = props;
  if (services.length === 0) {
    return null;
  }

  return (
    <div className="profile-container">
      <Header as="h1" inverted className="component-title">
        Services
      </Header>
      <div className="achievements-conatiner">
        <List bulleted>
          {services.map((service, index) => {
            return (
              <List.Item key={index} as="li">
                <strong>{service.title}</strong>
                <List.Item as='ul'>
                  <List.Item as='li' value='-'>
                    {service.description}
                  </List.Item>
                </List.Item>
              </List.Item>
            )
          })}
        </List>
      </div>
    </div>
  )
};
