import { Component } from 'react';
import { Header, Container, Grid, Modal } from 'semantic-ui-react';

export default (props) => {
  const { data = {} } = props;
  const { title, videos = [] } = data;
  return (
    <div className="component-container">
      <Container>
        <Grid stackable className="title-container">
          <Grid.Row>
            <Grid.Column computer={6} tablet={9} mobile={16}>
              <Header as="h1" className="component-title">
                {title} Videos
              </Header>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Grid stackable columns='equal' centered>
          <Grid.Row className="car-reviews-container">
            {videos.map((videoID, index) => {
              return (
                <Grid.Column computer={5} tablet={9} mobile={16} key={index}>
                  <VideoPlayer videoID={videoID} />
                </Grid.Column>
              )
            })}
          </Grid.Row>
        </Grid>
      </Container>
    </div>
  )
};

class VideoPlayer extends Component {
  state = {
    loadPlayer: false,
  }

  render() {
    const { thumbnail, videoID  } = this.props;
    const imageSrc = thumbnail || `https://img.youtube.com/vi/${videoID}/mqdefault.jpg`;

    return(
      <div className="videoplayer youtube" >
        <Modal trigger={
          <div>
            <img rel="preconnect" src={imageSrc} />
            <div className="play-button" />
          </div>
        }>
          <Modal.Content>
            <iframe width="100%" height="400" src={`https://www.youtube.com/embed/${videoID}`} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </Modal.Content>
        </Modal>
      </div>
    )
  }
}

