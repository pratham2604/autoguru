import React from 'react';
import Swiper from 'react-id-swiper';
import { Image } from 'semantic-ui-react';

const Slider = (props) => {
  const params = {
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    },
    loop: true,
  }
  const { images } = props;
  return (
    <Swiper {...params}>
      {images.map((imageSrc, index) => <div><Image src={imageSrc} key={index} className="profile-slider" /></div>)}
    </Swiper>
  )
};

export default Slider;