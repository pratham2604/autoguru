import { Header, Icon, Container, List } from 'semantic-ui-react';
import { Component } from 'react';

export default (props) => {
  const { faqs = [] } = props;
  if (!faqs.length) {
    return null;
  }

  return (
    <div className="component-container profile-component-container">
      <Container>
        <div className="profile-container">
          {faqs.map((qa, index) => <QAComponet data={qa} key={index}/>)}
        </div>
      </Container>
    </div>
  )
};

class QAComponet extends Component {
  state = {
    open: false,
  }

  toggle = () => {
    this.setState({
      open: !this.state.open
    });
  }

  render() {
    const { open } = this.state;
    const { question, answer, answerList = [], setHTML } = this.props.data || {};
    return (
      <div className="qa-component">
        <div className="question-container" onClick={this.toggle}>
          <div className="question">{question}</div>
          <div className="toggle-icon">
            {open ? <Icon name="chevron up"></Icon> : <Icon name="chevron down"></Icon>}
          </div>
        </div>
        {open &&
          <div className="answer-container" onClick={this.toggle}>
            <div dangerouslySetInnerHTML={{__html: `${answer}`}}></div>
            {answerList.length > 0 &&
              answerList.map((answer, index) => {
                return (
                  <List bulleted key={index}>
                    <List.Item>{answer}</List.Item>
                  </List>
                )
              })
            }
          </div>
        }
      </div>
    )
  }
}
