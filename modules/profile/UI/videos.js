import React, { Component, Fragment } from 'react';
import { Header, Container, Grid, Modal } from 'semantic-ui-react';
import Link from 'next/link';
import Swiper from '../../../components/swiper/swiper';

export default (props) => {
  const { videos = [], url } = props;

  if (videos.length === 0) {
    return null;
  }

  return (
    <div className="component-container profile-component-container">
      <Container>
        <Grid stackable className="title-container">
          <Grid.Row>
            <Grid.Column computer={2} tablet={9} mobile={16}>
              <Header as="h1" className="component-title">
                Videos
              </Header>
            </Grid.Column>
            <Grid.Column computer={2} tablet={3} mobile={16}>
              <div className="view-more-link">
                <Link href={`/partners/videos/${url}`}>
                  <a className="link">See all</a>
                </Link>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <div className="blogs-swiper-container">
          <Swiper>
            {videos.slice(0, 10).map((videoId, index) => (
              <VideoPlayer videoID={videoId} key={index} />
            ))}
          </Swiper>
        </div>
      </Container>
    </div>
  )
};

class VideoPlayer extends Component {
  state = {
    loadPlayer: false,
  }

  render() {
    const { thumbnail, videoID  } = this.props;
    const imageSrc = thumbnail || `https://img.youtube.com/vi/${videoID}/mqdefault.jpg`;

    return(
      <div className="videoplayer youtube" >
        <Modal trigger={
          <div>
            <img rel="preconnect" src={imageSrc} />
            <div className="play-button" />
          </div>
        }>
          <Modal.Content>
            <iframe width="100%" height="400" src={`https://www.youtube.com/embed/${videoID}`} frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </Modal.Content>
        </Modal>
      </div>
    )
  }
}
