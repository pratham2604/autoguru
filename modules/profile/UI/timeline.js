import { Header, List } from 'semantic-ui-react';
import { Component } from 'react';

export default class Timeline extends Component {
  state = {
    activeIndex: -1,
  }

  updateIndex = (index) => {
    const { activeIndex } = this.state;
    this.setState({
      activeIndex: index === activeIndex ? -1 : index,
    });
  }

  render() {
    const { timeline = [] } = this.props;
    const { activeIndex } = this.state;
    if (timeline.length === 0) {
      return null;
    }

    return (
      <div className="profile-container">
        <Header as="h1" inverted className="component-title">
          Timeline
        </Header>
        <div>
          {timeline.map((info, index) =>
            <MileStone info={info} updateIndex={this.updateIndex} activeIndex={activeIndex} index={index} key={index} />
          )}
        </div>
      </div>
    )
  }
}

class MileStone extends Component {
  render() {
    const { index, activeIndex, updateIndex, info } = this.props;
    const { title = '', year, events } = info || {};
    const open = index === activeIndex;
    return (
      <div>
        {!open ?
          <div className="milestone-container" onClick={updateIndex.bind(this, index)}>
            <div className="step completed">
              <div className="v-stepper">
                <div className="circle"></div>
                <div className="line"></div>
              </div>
              <div className="content">
                <Header as="h3" inverted>{title}</Header>
                <div>{year}</div>
              </div>
            </div>
          </div> :
          <div className="detailed-milestone-container" onClick={updateIndex.bind(this, index)}>
            <div className="content">
              <Header as="h3" inverted className="title">{title}</Header>
              <div className="year">{year}</div>
            </div>
            <List bulleted>
              {events.map((event, index) => <List.Item key={index}>{event}</List.Item>)}
            </List>
          </div>
        }
      </div>
    )
  }
}
