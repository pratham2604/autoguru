import { Header, List } from 'semantic-ui-react';

export default (props) => {
  const { achievements = [], achievementsTitle = '' } = props;
  return (
    <div className="profile-container">
      <Header as="h1" inverted className="component-title">
        Achievements
      </Header>
      <div className="achievements-conatiner">
        {achievementsTitle && <Header as="h3" inverted>{achievementsTitle}</Header>}
        <List bulleted>
          {achievements.map((achievement, index) => <List.Item key={index}>{achievement}</List.Item>)}
        </List>
      </div>
    </div>
  )
};
