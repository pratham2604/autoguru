import { Header, Icon, Grid, Image } from 'semantic-ui-react';

export default (props) => {
  const { data = {} } = props;
  const { logo, title, description, shortDescription, links } = data;
  return (
    <Grid stackable className="profile-details-container profile-container">
      <Grid.Row>
        <Grid.Column width={3}>
          <Image src={logo} className="logo" size="medium"/>
        </Grid.Column>
        <Grid.Column width={5}>
          <Header as="h1" inverted className="component-title">
            {title}
          </Header>
          <div className="short-desc">
            {shortDescription}
          </div>
          <div className="social-media">
            <a href={links.facebook} target="_blank">
              <Icon className="social-icon" name="facebook"/>
            </a>
            <a href={links.instagram} target="_blank">
              <Icon className="social-icon" name="instagram" />
            </a>
            <a href={links.linkedIn} target="_blank">
              <Icon className="social-icon" name="linkedin" />
            </a>
            <a href={links.twitter} target="_blank">
              <Icon className="social-icon" name="twitter"/>
            </a>
          </div>
        </Grid.Column>
        <Grid.Column width={8}>
          {description}
        </Grid.Column>
      </Grid.Row>
    </Grid>
  )
};