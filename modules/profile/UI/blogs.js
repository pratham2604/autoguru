import { Header, Container, Grid } from 'semantic-ui-react';
import Link from 'next/link';
import Swiper from '../../../components/swiper/swiper';
import Article from '../../../components/article/article';

export default (props) => {
  const { posts = [], slug = '' } = props;
  const blogPosts = posts.filter(post => {
    const { tags } = post;
    return tags.some(tag => tag.slug === slug);
  }).sort((a, b) => {
    const aDate = new Date(a.published_at).valueOf();
    const bDate = new Date(b.published_at).valueOf();
    return bDate - aDate;
  });

  if (blogPosts.length === 0) {
    return null;
  }

  return (
    <div className="component-container profile-component-container">
      <Container>
        <Grid stackable className="title-container">
          <Grid.Row>
            <Grid.Column computer={2} tablet={9} mobile={16}>
              <Header as="h1" className="component-title">
                Blogs
              </Header>
            </Grid.Column>
            {/* <Grid.Column computer={2} tablet={3} mobile={16}>
              <div className="view-more-link">
                <Link href={`/${slug}`}>
                  <a className="link">See all</a>
                </Link>
              </div>
            </Grid.Column> */}
          </Grid.Row>
        </Grid>
        <div className="blogs-swiper-container">
          <Swiper>
            {blogPosts.map((point, index) => (
              <Article
                key={index}
                link={point.url}
                image={point.feature_image}
                title={point.title}
                description={point.excerpt}
                published_at={point.published_at}
                reading_time={point.reading_time}
              />
            ))}
          </Swiper>
        </div>
      </Container>
    </div>
  )
};
